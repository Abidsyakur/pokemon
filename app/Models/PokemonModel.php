<?php

namespace App\Models;

use CodeIgniter\Model;

class PokemonModel extends Model
{
    protected $table = 'pokemons';
    protected $primaryKey = 'id';
    protected $allowedFields = ['name', 'skill', 'height', 'width', 'length', 'hp', 'damage', 'image'];
}
