<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pokemon Cards</title>
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css" rel="stylesheet">
    <style>

        body {
            background-color: #1a202c;
            display: flex;
            justify-content: center;
            color: #cbd5e0;
            align-items: center;
            min-height: 100vh;
        }

        .card-container {
            position: relative;
            width: 200px;
            height: 300px;
            overflow: hidden;
            perspective: 1000px;
        }

        .pokemon-card {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: #2d3748;
            border-radius: 10px;
            box-shadow: 0 0 20px rgba(255, 255, 255, 0.1);
            transition: transform 0.3s ease-in-out, background-color 0.5s ease-in-out;
            transform-style: preserve-3d;
        }
        .pokemon-card:hover {
            transform: rotateY(180deg);
            background-color: #f7d907;
            animation: blink 1s infinite;
        }

        @keyframes blink {
            0%, 100% {
                background-color: #2d3748;
            }
            50% {
                background-color: #f7d907;
            }
        }

        .pokemon-card .back {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            backface-visibility: hidden;
            transform: rotateY(180deg);
        }

        .ball-bar {
            background-color: #4a5568;
            border-radius: 50%;
            width: 50px;
            height: 50px;
            display: flex;
            align-items: center;
            justify-content: center;
            cursor: pointer;
            transition: background-color 0.3s ease-in-out;
            animation: bounce 1s infinite;
            position: fixed;
            bottom: 20px;
            right: 20px;
            z-index: 999;
        }

        @keyframes bounce {
            0%, 100% {
                transform: translateY(-20%);
            }
            50% {
                transform: translateY(0);
            }
        }

        .ball-bar:hover {
            background-color: #2c7be5;
            animation: none;
        }
    </style>
</head>

<body class="font-sans">
    <div class="container mx-auto py-8">
        <h1 class="text-3xl font-bold mb-40 text-center">Pokemon Cards</h1>
        <div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
            <!-- Card Containers -->
            <?php foreach ($pokemon as $poke) : ?>
                <div class="card-container">
                    <div class="pokemon-card p-4">
                        <div class="front">
                            <img src="/uploads/<?= $poke['image'] ?>" alt="<?= $poke['name'] ?>" class="mx-auto mb-4 w-20 h-20 object-cover">
                            <h2 class="text-lg text-yellow-300 font-semibold mb-2">Hero : <?= $poke['name'] ?></h2>
                            <p class="text-yellow-300 font-semibold mb-2">Skill : <?= $poke['skill'] ?></p>
                            <div class="mt-2 text-yellow-300 ">
                                <p><span >HP:</span> <?= $poke['hp'] ?></p>
                                <p><span >Damage:</span> <?= $poke['damage'] ?></p>
                            </div>
                        </div>
                        <!-- image  -->
                        <div class="back">
                            <img src="/uploads/<?= $poke['image'] ?>" alt="<?= $poke['name'] ?>" class="mx-auto mb-4 w-20 h-20 object-cover">
                            <h2 class="text-lg font-semibold mb-2">Back of <?= $poke['name'] ?></h2>
                        </div>
                        <!-- end of image -->
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <!--button nball bar -->
        <div class="ball-bar">
            <a href="<?= base_url('admin/create') ?>" class="text-white font-semibold">+</a>
        </div>
    </div>
</body>

</html>
