<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create Pokemon</title>
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css" rel="stylesheet">
    <style>
        body{
            background-color: #1a202c;
            color: #cbd5e0;
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
        }

        .form-input {
            background-color: #2d3748;
            border-color: #4a5568;
            color: #cbd5e0;
        }

        .form-input:focus {
            border-color: #2c7be5;
        }

        .form-input::placeholder {
            color: #a0aec0;
        }

        .btn-create {
            background-color: #2c7be5;
        }

        .btn-create:hover {
            background-color: #2b6cb0;
        }
    </style>
</head>

<body class="font-sans flex justify-center items-center h-screen">
    <div class="w-full max-w-md bg-gray-800 rounded-lg shadow-md p-8">
        <h1 class="text-3xl font-bold mb-8 text-center text-white">Create New Pokemon</h1>
        <form action="http://localhost:8080/admin/store" method="POST" enctype="multipart/form-data">
            <input type="text" id="name" name="name" class="form-input mb-4 w-full" placeholder="Name" required>
            <input type="text" id="skill" name="skill" class="form-input mb-4 w-full" placeholder="Skill" required>
            <div class="grid grid-cols-2 gap-4 mb-4">
                <input type="number" id="height" name="height" class="form-input w-full" step="0.01" min="0" placeholder="Enter height">
                <input type="number" id="width" name="width" class="form-input w-full" step="0.01" min="0" placeholder="Enter width">
                <input type="number" id="length" name="length" class="form-input w-full" step="0.01" min="0" placeholder="Enter length">              
                <input type="number" id="hp" name="hp" class="form-input w-full" placeholder="HP" required>
                <input type="number" id="damage" name="damage" class="form-input w-full" placeholder="Damage" required>
            </div>
            <input type="file" id="image" name="image" class="form-input mb-4 w-full" accept="image/*" required>
            <div class="flex justify-end">
                <button type="submit" class="btn-create text-white font-semibold py-2 px-4 rounded">Create</button>
            </div>
        </form>
    </div>
</body>

</html>
