<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\PokemonModel;
// Load session helper

class Admin extends BaseController
{
    protected $pokemonModel;

    public function __construct()
    {
        $this->pokemonModel = new PokemonModel();
    }

    //fungsi read
     public function index()
    {
        $pokemonModel = new PokemonModel();
        $data['pokemon'] = $pokemonModel->findAll();

        return view('admin/Pokemon/index', $data);
    }


    public function create()
    {
        return view('admin/pokemon/create');
    }


//function create
  public function store()
{
    // Ambil file gambar dari formulir
    $imageFile = $this->request->getFile('image');

    // Pindahkan file ke direktori yang diinginkan
    if ($imageFile->isValid() && $imageFile->move(ROOTPATH . 'public/uploads', $imageFile->getName())) {
        // Jika berhasil, simpan nama file ke database
        $imageName = $imageFile->getName();

        $pokemonModel = new PokemonModel();

        $data = [
            'name' => $this->request->getPost('name'),
            'skill' => $this->request->getPost('skill'),
            'height' => $this->request->getPost('height'),
            'width' => $this->request->getPost('width'),
            'length' => $this->request->getPost('length'),
            'hp' => $this->request->getPost('hp'),
            'damage' => $this->request->getPost('damage'),
            'image' => $imageName, // Simpan nama file ke database
        ];

        $pokemonModel->insert($data);

         // Set flash data sukses
        return redirect()->to('/admin'); // Redirect to dashboard after storing data
    } else {
        // Jika gagal, tampilkan pesan error
        $error = $imageFile->getError();
        return redirect()->back()->with('error', $error);
    }
}

//function edit atau update 
    public function edit($id)
{
    $pokemonModel = new PokemonModel();
    $pokemon = $pokemonModel->find($id);

    if ($pokemon) {
        // Data ditemukan, tampilkan formulir edit
        return view('admin/Pokemon/edit', ['pokemon' => $pokemon]);
    } else {
        // Data tidak ditemukan, redirect ke halaman lain atau tampilkan pesan error
        return redirect()->to('/admin')->with('error', 'Data Pokemon tidak ditemukan.');        
    }
}
public function update($id)
{
    // Ambil file gambar dari formulir
    $imageFile = $this->request->getFile('image');

    // Pindahkan file ke direktori yang diinginkan
    if ($imageFile->isValid() && $imageFile->move(ROOTPATH . 'public/uploads', $imageFile->getName())) {
        // Jika berhasil, simpan nama file ke database
        $imageName = $imageFile->getName();

        $pokemonModel = new PokemonModel();

        $data = [
            'name' => $this->request->getPost('name'),
            'skill' => $this->request->getPost('skill'),
            'height' => $this->request->getPost('height'),
            'width' => $this->request->getPost('width'),
            'length' => $this->request->getPost('length'),
            'hp' => $this->request->getPost('hp'),
            'damage' => $this->request->getPost('damage'),
            'image' => $imageName, // Simpan nama file ke database
        ];

        $pokemonModel->update($id, $data);

        // Set flash data sukses
        return redirect()->to('/admin')->with('success', 'Data Pokemon berhasil diperbarui.'); // Redirect to dashboard after updating data
    } else {
        // Jika gagal, tampilkan pesan error
        $error = $imageFile->getError();
        return redirect()->back()->with('error', $error); // Redirect back with error message
    }
}

public function destroy($id){
    $pokemonModel = new PokemonModel();
    $pokemon = $pokemonModel->find($id);
    if ($pokemon) {
        $pokemonModel->delete();
        return redirect()->to('/admin')->with('success','');
        }else{
            return redirect()->to('/admin')->with('error', '');
        }
    }
}
