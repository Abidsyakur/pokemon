<?php

namespace App\Controllers\User;

//load session helper
use App\Controllers\BaseController;
use App\Models\PokemonModel;

class User extends BaseController
{
    public function index(){
        $pokemonmodel = new PokemonModel();
        $data['pokemon'] = $pokemonmodel->findAll();
        return view('user/Pokemon/index', $data);
    }
}

